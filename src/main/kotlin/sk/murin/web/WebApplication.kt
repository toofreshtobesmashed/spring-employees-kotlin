package sk.murin.web

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
//.\gradlew bootRun
@SpringBootApplication
class WebApplication

fun main(args: Array<String>) {
	runApplication<WebApplication>(*args)
}

