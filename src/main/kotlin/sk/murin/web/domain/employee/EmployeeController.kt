package sk.murin.web.domain.employee

import org.springframework.stereotype.Controller
import org.springframework.stereotype.Repository
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import sk.murin.web.data.employee.EmployeeRepository
import sk.murin.web.data.employee.Employee
import sk.murin.web.data.employee.EmployeeRole

@Controller
class EmployeeController(val repository: EmployeeRepository) {
    @GetMapping("/employee")
    fun employee(@RequestParam(required = false, defaultValue = "0") id: Long, model: Model): String {

        model.addAttribute("roles", EmployeeRole.values())//takto sa mapuje enum
        if (id == 0L) {
            model.addAttribute("employee", Employee())
        } else {
            val employee = repository.findById(id).orElse(Employee())
            model.addAttribute("employee", employee)
        }
        return "employee"
    }

    @PostMapping("/employee")
    fun employee(@ModelAttribute("employee") employee: Employee): String {
        repository.save(employee)
        return "redirect:/employees"
    }

    @GetMapping("/employees")
    fun employees(model: Model):String{
        model.addAttribute("employees",repository.findAll())//vrati vsetkych zamestnancov
        return "employees"
    }

}