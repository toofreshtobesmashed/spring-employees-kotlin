package sk.murin.web.data.employee

import org.springframework.data.jpa.repository.JpaRepository
import sk.murin.web.data.employee.Employee

interface EmployeeRepository:JpaRepository<Employee,Long>