package sk.murin.web.data.employee

import java.sql.Date
import javax.persistence.*

@Entity
class Employee {
    @Id
    @GeneratedValue
    var id = 0L
    var name = ""
    var dateOfBirth = Date(System.currentTimeMillis())

    @Enumerated(EnumType.STRING)
    var role = EmployeeRole.NONE
    var salary = 0


}